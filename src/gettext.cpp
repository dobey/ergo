/*
 * Copyright 2012-2015 Canonical Ltd.
 * Copyright 2018 Rodney Dawes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "gettext.h"
#include "utils.h"

#include <QDebug>
#include <QDir>

#ifndef ANDROID
#include <libintl.h>
#endif


namespace ergo
{

//! @cond
Gettext::Gettext(QObject *parent):
    QObject(parent)
{
}
//! @endcond

/*! Get the translated version of a string
 *
 * @param text The untranslated version of a string to be translated
 * @return The translated version of text
 */
QString Gettext::tr(const QString& text) const
{
#if ANDROID
    return text;
#else
    return QString::fromUtf8(gettext(text.toUtf8()));
#endif
}

/*! Get the translated version of a possibly pluralized string
 *
 * @param singular The untranslated version of the string to be translated,
 *     in singular form
 * @param plural The untranslated version of the string to be translated,
 *     in plural form
 * @param n The value for determining the correct singular or plural form of
 *     the translated version of the string
 * @return The translated version of singular or plural
 */
QString Gettext::tr(const QString& singular,
                    const QString& plural,
                    int n) const
{
#if ANDROID
    return n == 1 ? singular : plural;
#else
    return QString::fromUtf8(ngettext(singular.toUtf8(), plural.toUtf8(), n));
#endif
}

//!@cond
QString Gettext::domain() const
{
    return m_domain;
}

void Gettext::setDomain(const QString& domain)
{
    if (!m_domain.isEmpty()) {
        qWarning() << "Domain already set, not setting.";
        return;
    }
    m_domain = domain;

#if ANDROID
    return;
#else
    textdomain(m_domain.toUtf8());

    /* Find the path */
    auto appDir = DirUtils::appPackageDir();
    QString localePath;

    if (!appDir.empty() && QDir::isAbsolutePath(appDir.c_str())) {
        localePath = QDir(appDir.c_str()).filePath(QStringLiteral("share/locale"));
    } else {
        localePath = QStringLiteral("/usr/share/locale");
    }

    bindtextdomain(m_domain.toUtf8(), localePath.toUtf8());
#endif
}
//! @endcond

} // namespace ergo
